package com.juliengenoud.revolutrates

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity

fun FragmentActivity.hideKeyboard() {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Context.getStringResourceByName(string: String): String? {
    val resId = resources.getIdentifier(string, "string", packageName)
    return try {
        getString(resId)
    } catch (e: Resources.NotFoundException) {
        null
    }
}

fun Context.getImageResourceByName(name: String): Drawable? {

    // todo : add all the missing flags ressources
    val unavailableFlagsRessources = arrayOf(
        "ic_hkd", "ic_krw", "ic_thb", "ic_rub",
        "ic_inr", "ic_isk", "ic_jpy", "ic_huf", "ic_ils", "ic_try", "ic_hrk", "ic_zar", "ic_idr",
        "ic_pln", "ic_sek", "ic_sgd", "ic_mxn", "ic_myr", "ic_nok", "ic_php", "ic_nzd", "ic_ron")
    if (unavailableFlagsRessources.contains(name)) {
        return null // avoid log exception
    }

    val resources = resources
    val resourceId = resources.getIdentifier(
        name, "drawable",
        packageName
    )

    return try {
        resources.getDrawable(resourceId)
    } catch (e: Resources.NotFoundException) {
        null
    }
}
