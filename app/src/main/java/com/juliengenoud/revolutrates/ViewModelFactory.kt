package com.juliengenoud.revolutrates

import android.annotation.SuppressLint
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.juliengenoud.revolutrates.data.RatesRepository
import com.juliengenoud.revolutrates.data.remote.RatesRemoteDataSource
import com.juliengenoud.revolutrates.rates.RatesViewModel

class ViewModelFactory private constructor(
    private val tasksRepository: RatesRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(RatesViewModel::class.java) ->
                    RatesViewModel(tasksRepository)
                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile private var INSTANCE: ViewModelFactory? = null

        fun getInstance() =
            INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                INSTANCE ?: ViewModelFactory(
                    RatesRepository.getInstance(
                        RatesRemoteDataSource.getInstance()))
                        .also { INSTANCE = it }
            }

        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }
    }



}

