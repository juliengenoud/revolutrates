package com.juliengenoud.revolutrates

object Defaults {
    const val DECIMAL_POINT: Int = 100
    const val REFRESH_DELAY: Long = 1000
    const val INPUT: Double = 1.0
    const val CURRENCY: String = "EUR"
}
