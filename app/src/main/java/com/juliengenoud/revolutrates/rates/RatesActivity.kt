package com.juliengenoud.revolutrates.rates

import android.animation.Animator
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.juliengenoud.revolutrates.R
import com.juliengenoud.revolutrates.ViewModelFactory


class RatesActivity : AppCompatActivity() {

    private lateinit var viewModel: RatesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = obtainViewModel()

        supportFragmentManager.beginTransaction().apply {
            val fragment = supportFragmentManager.findFragmentById(R.id.contentFrame)
                ?: RatesFragment.newInstance()
            replace(R.id.contentFrame, fragment)
        }.commit()

        val readyObserver = Observer<Boolean> { ready ->
            if (ready) {
                val logo = findViewById<View>(R.id.logo)
                logo.postDelayed({
                    logo.animate()
                        .setDuration(600)
                        .alpha(0.0f)
                        .setListener(object : Animator.AnimatorListener {
                            override fun onAnimationRepeat(animation: Animator?) {}

                            override fun onAnimationEnd(animation: Animator?) {
                                logo.visibility = View.GONE
                            }

                            override fun onAnimationCancel(animation: Animator?) {}

                            override fun onAnimationStart(animation: Animator?) {
                                findViewById<View>(R.id.contentFrame).visibility = View.VISIBLE
                            }
                        }).start()
                }, 2000)
            }
        }

        viewModel.dataLoading.observe(this, readyObserver)
    }

    fun obtainViewModel(): RatesViewModel = ViewModelProviders.of(
        this,
        ViewModelFactory.getInstance()
    ).get(RatesViewModel::class.java)
}
