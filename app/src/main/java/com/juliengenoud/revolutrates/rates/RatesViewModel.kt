package com.juliengenoud.revolutrates.rates

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.juliengenoud.revolutrates.Defaults
import com.juliengenoud.revolutrates.ViewModelWithScope
import com.juliengenoud.revolutrates.data.Rates
import com.juliengenoud.revolutrates.data.RatesRepository
import kotlinx.coroutines.*
import kotlin.math.round
import kotlin.reflect.full.memberProperties

class RatesViewModel(
    private val rateRepository: RatesRepository
) : ViewModelWithScope() {

    private lateinit var job: Job

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean>
        get() = _dataLoading

    private val _dataRate = MutableLiveData<List<Pair<String, Double>>>()
    val dataRate: LiveData<List<Pair<String, Double>>>
        get() = _dataRate

    private val _shouldRefresh = MutableLiveData<Boolean>()
    val shouldRefresh: LiveData<Boolean>
        get() = _shouldRefresh

    private val _showError = MutableLiveData<Boolean>()
    val showError: LiveData<Boolean>
        get() = _showError

    private val _currency = MutableLiveData<String>()
    val currency: LiveData<String>
        get() = _currency

    private val _inputNumber = MutableLiveData<Double>()
    val inputNumber: LiveData<Double>
        get() = _inputNumber

    fun setDefaultInput(input: Double) {
        _inputNumber.value = input
    }

    fun setCurrency(currency: String) {
        _currency.value = currency
    }

    fun shouldRefresh(shouldRefresh: Boolean) {
        _shouldRefresh.value = shouldRefresh
    }

    fun scheduleRateUpdate() {
        // cancel job when activity rotate or recreate
        if (::job.isInitialized && job.isActive)
            job.cancel()

        _showError.value = false
        _shouldRefresh.value = true
        job = uiScope.launch(Dispatchers.IO) {
            while (shouldRefresh.value == true) {
                withContext(Dispatchers.Main) {
                    _dataLoading.value = true
                }
                val rates =
                    rateRepository.getRatesAsync(_currency.value ?: Defaults.CURRENCY)
                processRates(rates)
                delay(Defaults.REFRESH_DELAY)
            }
        }

    }

    private suspend fun processRates(rates: Deferred<Rates>) {
        try {
            val rate = rates.await()
            val map: MutableList<Pair<String, Double>> = mutableListOf()

            val inputMultiplier = inputNumber.value ?: Defaults.INPUT

            // use reflection to list all the currency and put them in a list of pairs
            Rates.Rate::class.memberProperties.forEach {
                map.add(
                    Pair(
                        it.name, round(
                            it.get(rate.rates) as Double
                                    * inputMultiplier * Defaults.DECIMAL_POINT)
                                / Defaults.DECIMAL_POINT
                    )
                )
            }

            currency.value?.let {
                // remove useless value
                map.find { pre -> it == pre.first }?.let { eur -> map.remove(eur) }
                map.add(0, Pair(it, inputMultiplier))
            }

            withContext(Dispatchers.Main) {
                _dataRate.value = map
                _dataLoading.value = false
            }
        } catch (e: Exception) {
            // need a better Exception Handler
            withContext(Dispatchers.Main) {
                _shouldRefresh.value = false
                _showError.value = true
                _dataLoading.value = false
            }
            e.printStackTrace()
        }
    }
}

