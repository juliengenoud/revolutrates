package com.juliengenoud.revolutrates.rates

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.juliengenoud.revolutrates.R
import com.juliengenoud.revolutrates.getImageResourceByName
import com.juliengenoud.revolutrates.getStringResourceByName
import java.util.*


class RateAdapter(
    var callback: RateAdaperCallback,
    var context: Context?
) : RecyclerView.Adapter<RateAdapter.MyViewHolder>() {

    interface RateAdaperCallback {
        fun scrollToTop()
        fun updateViewModel(newCurrency: String, newInput: Double)
        fun updateViewModelInput(newInput: Double)
    }

    var mCurrencyList: MutableList<Pair<String, Double>> = mutableListOf()

    class MyViewHolder(v: View, callback: RateAdaperCallback) :
        RecyclerView.ViewHolder(v) {
        var mEditCurrency: EditText = v.findViewById(R.id.editTextCurrency)
        var mTextCurrShort: TextView = v.findViewById(R.id.textViewCurrShort)
        var mTextCurrLong: TextView = v.findViewById(R.id.textViewCurrLong)
        var mFlagImageView: ImageView = v.findViewById(R.id.flagImg)

        init {
            this.mEditCurrency.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    if (adapterPosition == 0) {
                        if (s.toString().isEmpty()) {
                            callback.updateViewModelInput(0.0)
                            s?.append("0")
                        }

                        if (s?.get(0) == '0' && s.length > 1)
                            s.delete(0, 1)
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                   // do nothing ...
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    try {
                        if (adapterPosition == 0) {
                            callback.updateViewModelInput(
                                s.toString().replace(",", ".").toDouble()
                            )
                        }
                    } catch (e: NumberFormatException) {
                        // not ready to be parsed
                    }
                }
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val textView = LayoutInflater.from(parent.context)
            .inflate(R.layout.data_view, parent, false)
        return MyViewHolder(textView, callback)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.mTextCurrShort.text = mCurrencyList[position].first
        holder.mTextCurrLong.text = context?.getStringResourceByName(
            mCurrencyList[position].first
        ) ?: "Unknown Currency"

        context?.let {
            holder.mFlagImageView.setImageDrawable(
                it.getImageResourceByName(
                    "ic_" +
                            mCurrencyList[position].first.toLowerCase()
                )
                    ?: it.resources.getDrawable(R.drawable.ic_usd)
            )
        }

        val currency = mCurrencyList[position]
            .second.toString().replace('.', ',')
        holder.mEditCurrency.setText(currency)
        holder.mEditCurrency.setTextColor(Color.BLACK)

        if (currency == "0,0")
            holder.mEditCurrency.setTextColor(Color.GRAY)
        //  holder.mEditCurrency.removeTextChangedListener(listener)

        if (holder.adapterPosition != 0) {
            holder.mEditCurrency
                .setOnTouchListener { v, event ->
                    handleView(holder, v)
                    v?.requestFocus()
                    (v as EditText).setTextColor(Color.BLACK)
                    val imm =
                        context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm?.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT)
                    v.setOnTouchListener(null)
                    true
                }

            holder.itemView.setOnClickListener { v ->
                if (holder.adapterPosition != 0) {
                    handleView(holder, v)
                }
            }
        } else {
            holder.mEditCurrency.setOnTouchListener(null)
        }


    }

    private fun handleView(holder: MyViewHolder, v: View?) {
        callback.updateViewModel(
            mCurrencyList[holder.adapterPosition].first,
            mCurrencyList[holder.adapterPosition].second
        )
        Collections.swap(this.mCurrencyList, holder.adapterPosition, 0)
        notifyItemMoved(holder.adapterPosition, 0)
        callback.scrollToTop()
    }

    override fun getItemCount() = mCurrencyList.size
}