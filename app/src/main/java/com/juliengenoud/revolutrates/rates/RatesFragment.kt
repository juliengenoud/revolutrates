package com.juliengenoud.revolutrates.rates

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.juliengenoud.revolutrates.Defaults
import com.juliengenoud.revolutrates.hideKeyboard

class RatesFragment : Fragment() {

    private lateinit var viewModel: RatesViewModel

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RateAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(com.juliengenoud.revolutrates.R.layout.rates_frag,
            container, false)
        recyclerView = view as RecyclerView
        setupRecyclerView()
        viewModel = (activity as RatesActivity).obtainViewModel()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setDefaultInput(Defaults.INPUT)
        viewModel.setCurrency(Defaults.CURRENCY)
        viewModel.scheduleRateUpdate()
        setupObservers()
    }

    override fun onPause() {
        super.onPause()
        viewModel.shouldRefresh(false)
    }
    override fun onResume() {
        super.onResume()
        viewModel.scheduleRateUpdate()
    }

    private val callback: RateAdapter.RateAdaperCallback = object : RateAdapter.RateAdaperCallback {

        override fun updateViewModelInput(newInput: Double) {
            viewModel.setDefaultInput(newInput)
        }

        override fun scrollToTop() {
            recyclerView.post {
                viewManager.scrollToPosition(0)
            }
        }

        override fun updateViewModel(newCurrency:String, newInput:Double) {
            viewModel.setDefaultInput(newInput)
            viewModel.setCurrency(newCurrency)
        }

    }

    private fun setupRecyclerView() {
        viewManager = LinearLayoutManager(requireActivity())
        viewAdapter = RateAdapter(callback, context)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val firstVisiblePosition = (viewManager as LinearLayoutManager).findFirstVisibleItemPosition()
                if (firstVisiblePosition  != 0) {
                    activity?.hideKeyboard()
                }
            }
        })

        (recyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
    }

    private fun setupObservers() {
        var i = 0

        val rateObserver = Observer<List<Pair<String,Double>>> { newRates ->
            i++
            viewAdapter.mCurrencyList = newRates.toMutableList()
            if (i == 1)
                viewAdapter.notifyDataSetChanged()
            else
                viewAdapter.notifyItemRangeChanged(1, newRates.size)
        }

        val errorObserver = Observer<Boolean> { error ->
            if (error == true) {
                showAlertMessage()
            }
        }

        viewModel.dataRate.observe(this, rateObserver)
        viewModel.showError.observe(this, errorObserver)
    }

    private fun showAlertMessage() {
        MaterialAlertDialogBuilder(context)
            .setTitle(getString(com.juliengenoud.revolutrates.R.string.retry_title))
            .setMessage(getString(com.juliengenoud.revolutrates.R.string.retry_message))
            .setCancelable(false)
            .setPositiveButton(
                com.juliengenoud.revolutrates.R.string.retry_button
            ) { _, _ ->
                viewModel.scheduleRateUpdate()
            }
            .show()
    }

    companion object {
        fun newInstance() = RatesFragment()
    }
}