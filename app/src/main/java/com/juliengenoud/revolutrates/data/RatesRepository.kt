package com.juliengenoud.revolutrates.data

import kotlinx.coroutines.Deferred

class RatesRepository(
    private val ratesRemoteDataSource: RatesDataSource
) : RatesDataSource {

    override suspend fun getRatesAsync(targetRate :String) : Deferred<Rates> {
        return ratesRemoteDataSource.getRatesAsync(targetRate)
    }

    companion object {

        private var INSTANCE: RatesRepository? = null

        @JvmStatic fun getInstance(ratesRemoteDataSource: RatesDataSource) =
            INSTANCE ?: synchronized(
                RatesRepository::class.java) {
                INSTANCE
                    ?: RatesRepository(
                        ratesRemoteDataSource
                    )
                    .also { INSTANCE = it }
            }

        @JvmStatic fun destroyInstance() {
            INSTANCE = null
        }
    }
}
