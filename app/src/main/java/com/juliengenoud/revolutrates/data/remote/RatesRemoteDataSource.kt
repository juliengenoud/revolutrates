package com.juliengenoud.revolutrates.data.remote

import androidx.annotation.VisibleForTesting
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.juliengenoud.revolutrates.data.Rates
import com.juliengenoud.revolutrates.data.RatesDataSource
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RatesRemoteDataSource : RatesDataSource {

    private val service: RatesService

    init {
        val client = OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .writeTimeout(5, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl("https://revolut.duckdns.org/")
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        service = retrofit.create(RatesService::class.java)
    }

    override suspend fun getRatesAsync(targetRate: String): Deferred<Rates> {
        return service.getRatesAsync(targetRate)
    }

    companion object {
        private var INSTANCE: RatesRemoteDataSource? = null

        @JvmStatic
        fun getInstance(): RatesRemoteDataSource {
            if (INSTANCE == null) {
                synchronized(RatesRemoteDataSource::javaClass) {
                    INSTANCE =
                        RatesRemoteDataSource()
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}
