package com.juliengenoud.revolutrates.data.remote

import com.juliengenoud.revolutrates.data.Rates
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesService {
    @GET("latest")
    fun getRatesAsync(@Query("base") base: String): Deferred<Rates>
}