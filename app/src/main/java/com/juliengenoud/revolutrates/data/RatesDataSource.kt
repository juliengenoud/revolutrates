package com.juliengenoud.revolutrates.data

import kotlinx.coroutines.Deferred

interface RatesDataSource {
    suspend fun getRatesAsync(targetRate :String): Deferred<Rates>
}
