# Revolut Rates

### App

List all currencies you get from Revolut Api and present them in a Android View

### Tech

Use MVVM and Android ViewModels based on [googlesamples/todo-mvvm-live-kotlin](https://github.com/googlesamples/android-architecture/tree/todo-mvvm-live-kotlin)

### Libraries :

 - material
 - recyclerView
 - appCompat
 - archLifecycle
 - coroutines
 - retrofit

       
License
----

MIT

![](https://bitbucket.org/juliengenoud/revolutrates/downloads/screenshot.png)
